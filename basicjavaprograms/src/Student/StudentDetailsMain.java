package Student;

public class StudentDetailsMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StudentDetails s1 = new StudentDetails("pradeep", 99, 95, 97);
		StudentDetails s2 = new StudentDetails("kohli", 57, 65, 72);
		StudentDetails s3 = new StudentDetails("sachin", 60, 80, 72);

		System.out.println(s1.toString());
		System.out.println(s2.toString());
		System.out.println(s3.toString());

		/*
		 * System.out.println(s1); System.out.println(s2);
		 * System.out.println(s3);
		 */

		System.out.println();
		System.out.println("===compare two student====");
		StudentDetails s = StudentDetails.compare(s1, s2);
		System.out.println(s);

		System.out.println();
		System.out.println("====compare three students=====");
		System.out.println(StudentDetails.compare(s1, StudentDetails.compare(s2, s3)));

	}

}
