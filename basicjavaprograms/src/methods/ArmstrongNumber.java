package methods;

import java.util.Scanner;

public class ArmstrongNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int start = 100, limit = 10000;
		int num = 8208;
		
		
		System.out.println(isArmstrong(num));
        System.out.println(getRange(start, limit));
	}

	
	public static String getRange(int start, int limit) {
		String str = "";
		for (int i = start; i <= limit; i++) {
			if (isArmstrong(i)) {
				str += i + " ";
			}

		}
		return str;
	}
	

	public static int countDigits(int num) {
		int count = 0;
		while (num > 0) {
			num = num / 10;
			++count;
		}
		return count;
	}

	
	public static boolean isArmstrong(int num) {
		int sum = 0, digit = 0;
		int temp = num;
		int digitCount = countDigits(num);
		
		while (num > 0) {
			digit = num % 10;
			sum += Math.pow(digit, digitCount);
			num = num / 10;
	    }

		if (temp == sum)
			return true;
		else
			return false;
	}
}
