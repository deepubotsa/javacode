package methods;

public class Sample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 10, num2 = 4, num3 = 29;
		int max = num1;
		if (max < num2)
			System.out.println(num2);
		else if (max < num3)
			System.out.println(num3);
		else
			System.out.println(num1);

	}

}
