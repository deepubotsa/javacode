package methods;

public class TwinPrime {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int start = 1;
		int limit = 40;

		System.out.println(twinPrimes(start, limit));
	}

	public static String twinPrimes(int start, int limit) {

		String s = "";
		int diff = 2;
		int count = 0;

		if (start >= limit)
			return "-2";

		if (start <= 0 || limit > 40)
			return "-1";

		for (int i = start; i <= limit; i++) {

			if (isPrime(i)) {

				if (isPrime(i - diff)) {

					s += (i - diff) + ":" + i + " ";
					count += 1;

				}
			}

		}
		if (count < 1)
			return "-3";

		return s;

	}

	public static boolean isPrime(int num) {
		int count = 0;
		if (num == 1)
			return false;
		if (num == 2)
			return false;

		for (int i = 2; i < num; i++) {
			if (num % i == 0)
				count += 1;
		}

		if (count < 2)
			return true;
		else
			return false;

	}
}