package methods;

public class GradeOfTheStudent {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sub1 = 100, sub2 = 95, sub3 = 90;

		// double persentage = (sub1 + sub2 + sub3) / 3;
		// System.out.println(persentage);
		System.out.println("persentage is:" + calPersentage(sub1, sub2, sub3));
		System.out.println("grade is:" + getGrade(calPersentage(sub1, sub2, sub3)));
	}

	public static double calPersentage(int sub1, int sub2, int sub3) {
		double persentage = (sub1 + sub2 + sub3) / 3;
		return persentage;

	}

	public static char getGrade(double persentage) {
		if (persentage >= 90)
			return 'A';
		else if (persentage >= 70 && persentage < 90)
			return 'B';
		else if (persentage >= 50 && persentage < 70)
			return 'C';
		else
			return 'F';

	}

}
