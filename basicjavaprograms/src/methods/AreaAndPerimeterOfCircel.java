package methods;

public class AreaAndPerimeterOfCircel {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double p = 3.14;
		int radius = 4;
		System.out.println("area of circel is:" + areaOfCircel(p, radius));
		System.out.println("perimeter of circel is:" + perimeterOfCircel(p, radius));

	}

	public static double areaOfCircel(double p, int radius) {
		double area = p * radius * radius;
		return area;
	}

	public static double perimeterOfCircel(double p, int radius) {
		double perimeter = 2 * p * radius;
		return perimeter;
	}

}
