package methods;

public class CheckingDigitEvenOrOdd {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 34567;
		System.out.println(checkDigitEvenOrOdd(num));

	}

	public static String checkDigitEvenOrOdd(int num) {
		String result = "";
		int evenCount = 0, oddCount = 0, digit;
		while (num > 0) {
			digit = num % 10;
			if (digit % 2 == 0)
				evenCount++;

			else
				oddCount++;

			num = num / 10;
		}
		return "no of even digits:"+ evenCount + " " +"no of odd digits:"+ oddCount;

	}
}
