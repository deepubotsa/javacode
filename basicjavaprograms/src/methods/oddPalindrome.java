package methods;

public class oddPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int start = 1500, limit = 2000;

		System.out.println(generatePalindrome(start, limit));
		// System.out.println(isAllDigitsOdd(num));
		// System.out.println(isPalindrome(num));

	}

	public static String generatePalindrome(int start, int limit) {
		String str = "";
		if(start <= 0 || limit <= 0)
			return "-1";
		
		if(start > limit)
			return "-2";
		
		for (int i = start; i <= limit; i++) {
			
			if (isPalindrome(i)) {
				
				if (isAllDigitsOdd(i))
					str += i + " ";
				else
					return "-3";
			}

		}
		return str;
	}

	public static boolean isAllDigitsOdd(int num) {
		int digit = 0;
		String str = "";
		while (num > 0) {
			digit = num % 10;
			if (digit % 2 == 0)
				return false;
			num = num / 10;
		}

		return true;

	}

	public static boolean isPalindrome(int num) {
		int sum = 0, digit = 0;
		int temp = num;
		while (num > 0) {
			digit = num % 10;
			sum = sum * 10 + digit;
			num = num / 10;
		}

		if (temp == sum)
			return true;
		else
			return false;

	}

}
