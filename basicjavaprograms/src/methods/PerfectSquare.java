package methods;

public class PerfectSquare {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int start = 1000;
		int limit = 9999;

		System.out.println(getPerfectSquareNums(start, limit));

	}

	public static String getPerfectSquareNums(int start, int limit) {
		String s = "";

		for (int i = start; i <= limit; i++) {
			if (isPerfectSquare(i)) {
				if (isAllDigitsEven(i)) {
					s += i + " ";
				}
			}
		}

		return s;
	}

	public static boolean isAllDigitsEven(int num) {
		String s = "" + num;
		int len = s.length();
		int sum = 0;
		for (int i = 1; i <= len - 1; i ++) {
			if (s.charAt(i) % 2 == 0)
				sum += i;
		}
		if (sum % 2 == 0)
			return true;
		else
			return false;

	}

	public static boolean isPerfectSquare(int num) {
		int sqrt = (int) Math.sqrt(num);

		if (sqrt * sqrt == num) {
			return true;
		} else {
			return false;
		}

	}

}
