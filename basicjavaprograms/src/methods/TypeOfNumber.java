package methods;

public class TypeOfNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 23;
		System.out.println(sumOfProperDivisiors(num));

	}

	public static int sumOfProperDivisiors(int num) {
		int sum = 0;
		if(num < 0)
			return -2;
		
		if(num == 0)
			return -3;
		
		for (int i = 1; i < num; i++) {
			if (num % i == 0) {
				sum += i;
			}
		}
		if (num == sum)
			return 0;
		
		if(num > sum)
			return -1;
		
		if(num < sum)
			return 1;
		
		
		return num;

	}

}
