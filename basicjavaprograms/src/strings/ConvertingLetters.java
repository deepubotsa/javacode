package strings;

public class ConvertingLetters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "This Is a SAMPLE text!", res = "";
		char ch;
		
		for (int i = 0; i <= str.length() - 1; i++) {
			ch = str.charAt(i);
			
			
			if (Character.isUpperCase(ch)) {
				res += Character.toLowerCase(ch);
				
			} else if (Character.isLowerCase(ch)) {
				res += Character.toUpperCase(ch);
				
			} else {
				res += ch;
			}

		}
		System.out.println(res);

	}
}
