package strings;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the string");
		String s = scn.nextLine();
		String reverse = "";
		
		
		int length = s.length();

		for (int i = length - 1; i >= 0; i--) {
			reverse = reverse + s.charAt(i);
		}
	    System.out.println(reverse);

		if (s.equals(reverse))
			System.out.println("given string is palindrome");
		else
			System.out.println("given string is not a palindrome");

	}

}
