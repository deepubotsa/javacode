package strings;

public class CompareStrings {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str1 = "hello";
		String str2 = new String("hello");//create new object
		String str3 = new String("hello");
		
		//System.out.println("== " + (str1 == str2));//only compare addresses
		System.out.println("Equals method:"+str1.equals(str2));
		System.out.println("Equals method:"+str2.equals(str3));

	}

}
