package Interface;

public class WaterBill implements Bill {
	public static final int per_unit_charge = 5;
	private int no_of_units;


	public WaterBill() {
		super();
	}
	

	public WaterBill(int no_of_units) {
		super();
		this.no_of_units = no_of_units;
	}
	public int getNo_of_units() {
		return no_of_units;
	}
	public void setNo_of_units(int no_of_units) {
		this.no_of_units = no_of_units;
	}

	public static int getPerUnitCharge() {
		return per_unit_charge;
	}

	public  double calBill() {
		// TODO Auto-generated method stub
		return no_of_units *  per_unit_charge;
	}

	@Override
	public void displayBill() {
		// TODO Auto-generated method stub
		System.out.println("waterBills is:" + calBill());
		
	}

}
