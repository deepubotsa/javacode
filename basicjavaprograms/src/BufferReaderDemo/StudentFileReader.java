package BufferReaderDemo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StudentFileReader {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String line = null;
		FileReader fr = null;
		BufferedReader br = null;
		List<Student>stdList = new ArrayList<Student>();
		
		try {
			fr = new FileReader("student.csv");
			br = new BufferedReader(fr);
		
		while((line = br.readLine()) != null){
			System.out.println(line);
			String[] part = line.split(",");
			Student s = new Student(part[0],Integer.parseInt(part[1]),Integer.parseInt(part[2]),Integer.parseInt(part[3]));
			stdList.add(s);
			
		}
		Collections.sort(stdList);
		System.out.println("=====cal percentages======");
		for(Student s : stdList){
			System.out.println(s.getName() + " " + s.calPercentage());
		}
			
		}
		
		finally{
			fr.close();
			br.close();
		}
		

	}

}
