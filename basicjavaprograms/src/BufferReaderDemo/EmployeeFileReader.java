package BufferReaderDemo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EmployeeFileReader {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		String line = null;
		FileReader fr = null;
		BufferedReader br = null;
		
		List<Employee>empList = new ArrayList<Employee>();
	
		
		try {
			fr = new FileReader("Employee.txt");
			br = new BufferedReader(fr);
			
			while((line = br.readLine())!= null){
				System.out.println(line);
				String[] parts = line.split(" ");
				Employee emp = new Employee(Integer.parseInt(parts[0]), parts[1], Double.parseDouble(parts[2]), parts[3]);
				System.out.println(emp);
				empList.add(emp);
			}
			Collections.sort(empList);
			
			System.out.println("====list content======");
			for(Employee emp : empList){
				System.out.println(emp);
			}
			for(Employee emp : empList){
				System.out.println(emp.getName() + " " + emp.getSalary());
			}
		
		} 
		finally{
			if(fr != null)
			fr.close();
			br.close();
		}
		

	}

}
