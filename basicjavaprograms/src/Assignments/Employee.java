package Assignments;

public class Employee implements Comparable<Employee> {

	private int id;
	private String name;
	private double salary;

	public Employee() {
		super();

	}

	public Employee(String name, double salary) {
		super();

		this.name = name;
		this.salary = salary;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public int getId() {
		return id;
	}
	public int compareTo(Employee o) {
		// TODO Auto-generated method stub
		if(this.getSalary()>o.getSalary())
			return 1;
		else
		//return this.getName().compareTo(o.getName());
		return -1;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}

	

}
