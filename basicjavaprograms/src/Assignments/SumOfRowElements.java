package Assignments;

public class SumOfRowElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// int arr[][] = new int[3][3];
		int matrix[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		calSumOfRowElements(matrix);

	}

	public static void calSumOfRowElements(int[][] matrix) {
		// TODO Auto-generated method stub
		int sum = 0;
		for (int i = 0; i < matrix.length; i++) {

			sum = 0;

			for (int j = 0; j < matrix.length; j++) {

				System.out.print(matrix[i][j] + " ");
				sum += matrix[i][j];
			}

			System.out.print("sum is:" + sum);

			System.out.println();
		}

	}

}
