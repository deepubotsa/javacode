package Assignments;

import java.util.Scanner;

public class LargestNumberInArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int size = 0;
		
		Scanner scn = new Scanner(System.in);
		System.out.println("enter array size");
		size = scn.nextInt();

		System.out.println("enter the elements");
		int arr[] = new int[size];
		
		for (int i = 0; i < size; i++) {
			arr[i] = scn.nextInt();
		}
		
		System.out.println("largest num is:" + findLargestNum(arr));

	}

	private static int findLargestNum(int[] arr) {
		// TODO Auto-generated method stub
		int max = arr[0];
		

		for (int i = 1; i < arr.length; i++) {

			if (arr[i] > max)
				max = arr[i];

		}

		return max;
	}
}