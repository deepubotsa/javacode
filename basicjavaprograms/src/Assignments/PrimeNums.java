package Assignments;

import java.util.Scanner;

public class PrimeNums {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scn = new Scanner(System.in);
		System.out.println("enter starting number");
		int start = scn.nextInt();

		System.out.println("enter the limit");
		int limit = scn.nextInt();

		System.out.println(getPrimeNums(start, limit));
	}

	public static String getPrimeNums(int start, int limit) {
		String s = "";

		if (start <= 0 || start <= 0)
			return "Enter positive nums";

		// if (start == 1 || start == 1)
		// return "1 not a prime";

		for (int i = start; i <= limit; i++) {

			if (isPrime(i)) {

				s += i + " ";
			}

		}
		return s;
	}

	public static boolean isPrime(int num) {
		int count = 0;
		for (int i = 2; i < num; i++) {
			if (num % i == 0)
				count += 1;
		}
		return (count < 1);

		
	}

}
