package Assignments;

import java.util.Scanner;

public class SumOfMinNumsArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = { 5, 7, 9, 54, 1, 3 };

		System.out.println("sum of minimum nums is:" + sumOfMinNumbers(arr));
	}

	public static int sumOfMinNumbers(int arr[]) {
		int sum = 0;
		int min1 = arr[0];
		int min2 = arr[1];

		for (int i = 1; i < arr.length; i++) {

			if (arr[i] == min1) {
				min2 = min1;
			} else if (arr[i] < min1) {
				min2 = min1;
				min1 = arr[i];
			} else if (arr[i] < min2) {
				min2 = arr[i];
			}

			sum = min1 + min2;

		}
		return sum;
	}
}
