package Assignments;

public class StringReverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(reverseWords("Hello world"));
	}
	
	public static String reverseWords(String s){
		
		String rev1 = "",rev2 = "";
		int index = s.indexOf(" ");
		
		
		for(int i = index - 1; i >= 0;i --)
			rev1 +=  s.charAt(i);
		
		for(int i = s.length() - 1; i > index;i --)
		    rev2 += s.charAt(i);
		
		
		return rev1 + " " + rev2;
	}
	

}
