package Assignments;

import java.util.ArrayList;
import java.util.List;

public class CountEvenOddInArrayList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> list = new ArrayList<>();
		list.add(3);
		list.add(2);
		list.add(5);
		list.add(4);
		list.add(6);
		list.add(7);

		findUniqueNums(list);

	}

	public static void findUniqueNums(List<Integer> list) {
		// TODO Auto-generated method stub
		int evenCount = 0, oddCount = 0;
		for (Integer i : list) {
			if (i % 2 == 0) {
				evenCount += 1;

			} else {
				oddCount += 1;

			}

		}
		System.out.println("evenCount is:" + evenCount);
		System.out.println("oddCount is:" + oddCount);

	}

}
