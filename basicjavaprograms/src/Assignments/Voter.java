package Assignments;

public class Voter {
	private int id;
	private String name;
	private int age;
	public Voter() {
		super();
	}
	public Voter(int id, String name, int age) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Voter [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
	

}
