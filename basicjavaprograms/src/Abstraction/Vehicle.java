package Abstraction;

public abstract class Vehicle {
	private String brand;
	private String model;
	private int registration;
	public Vehicle() {
		super();
	}
	public Vehicle(String brand, String model, int registration) {
		super();
		this.brand = brand;
		this.model = model;
		this.registration = registration;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getRegistration() {
		return registration;
	}
	public void setRegistration(int registration) {
		this.registration = registration;
	}
	
	@Override
	public String toString() {
		return "Vehicle [brand=" + brand + ", model=" + model + ", registration=" + registration + "]";
	}
	public abstract String followSafety();
	public abstract String drive();

}
