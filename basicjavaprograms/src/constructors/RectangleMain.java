package constructors;

public class RectangleMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Rectangle obj1= new Rectangle(7,9.5,6);
		Rectangle obj2 = new Rectangle(3,7.7);
		Rectangle obj3 = new Rectangle(6,8.8);
		
		
		/*obj1.length = 7.5;;
		obj1.width = 9.5;
		
		obj2.length = 3.4;
		obj2.width = 7.7;
		
		obj3.length = 6.6;
		obj3.width = 8.8;*/
		
		
		System.out.println("area of rectangle:" + obj1.area());
		System.out.println("perimeter of rectangle:" + obj1.perimeter());
		
		System.out.println();
		
		
		System.out.println("area of rectangle2 is:" + obj2.area());
		System.out.println("perimeter of rectangle2 is:" + obj2.perimeter());
		
		System.out.println();
		
		
		System.out.println("area of rectangle2 is:" + obj3.area());
		System.out.println("perimeter of rectangle2 is:" + obj3.perimeter());
	}
	

}
