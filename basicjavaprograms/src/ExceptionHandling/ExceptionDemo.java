package ExceptionHandling;

public class ExceptionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 10;
		int b = 0;
		int sum = 0, div = 0, mul = 0;
		try {
			sum = a + b;
			div = a / b;
			mul = a * b;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		System.out.println(sum);
		System.out.println(mul);
		System.out.println(div);
	}

}
