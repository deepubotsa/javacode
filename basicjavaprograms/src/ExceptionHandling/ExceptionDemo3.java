package ExceptionHandling;

public class ExceptionDemo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			String s = null;
			System.out.println(s.length());
			int a[] = new int[5];
			a[5] = 30 / 0;
		} catch (ArithmeticException e) {
			System.out.println("cannot divide by 0!");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("check array index");
		} catch (Exception e) {
			System.out.println("some other exception");
		} finally {
			System.out.println("finally block executed");
		}
		System.out.println("rest of the code");

	}

}
