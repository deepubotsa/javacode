package Inheritance;

public class EmployeeDemo  {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Programmer p1 = new Programmer("Ajay", 25000, "Gachibowli", 8074018466l, "java");
		Programmer p2 = new Programmer("pradeep", 35000, "Gachibowli", 8074018466l, "java");
		
		System.out.println(p1);
		System.out.println(p2);
		p1.work();
		
		System.out.println("programmer :" + p1.getId());
		System.out.println("programmer :" + p2.getId());


	}

}
