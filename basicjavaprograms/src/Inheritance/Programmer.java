 package Inheritance;

public class Programmer extends Employee{

	private String programmingLanguage;
	
	public Programmer(){
		super();
	}
	
	public Programmer(String name, double salary, String address, long phoneNumber, String programmingLanguage) {
		super(name, salary, address, phoneNumber);
		this.programmingLanguage = programmingLanguage;
	}
	
	public String getProgrammingLanguage() {
		return programmingLanguage;
	}
	
	public void setProgrammingLanguage(String programmingLanguage) {
		this.programmingLanguage = programmingLanguage;
	}
	
	
	public void work(){
		System.out.println("Iam programmer! writing somecode..");
	}
	
	public String toString() {
		return "Programmer [programmingLanguage=" + programmingLanguage + ", getName()=" + getName() + ", getSalary()="
				+ getSalary() + ", getAddress()=" + getAddress() + ", getPhoneNumber()=" + getPhoneNumber()
				+ ", getId()=" + getId() + "]";
	}
	
	}
	

