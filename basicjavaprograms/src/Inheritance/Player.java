package Inheritance;

public abstract class Player {
	private int id;
	private int jerceyno;
	private String name;
	private int matches;
	private int idGenerator = 100;
	
	
	
	public Player( int id,int jerceyno, String name, int matches) {
		super();
		this.id = id;
		this.jerceyno = jerceyno;
		this.name = name;
		this.matches = matches;
		this.id = ++idGenerator;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getJerceyno() {
		return jerceyno;
	}
	public void setJerceyno(int jerceyno) {
		this.jerceyno = jerceyno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMatches() {
		return matches;
	}
	public void setMatches(int matches) {
		this.matches = matches;
	}
	public int getIdGenerator() {
		return idGenerator;
	}
	
	
	public String toString() {
		return "Player [id=" + id + ", jerceyno=" + jerceyno + ", name=" + name + ", matches=" + matches + "]";
	}
	public abstract double calAverage();
}
