package Inheritance;

public class Employee {
	private int id;
	private String name;
	private double salary;
	private String address;
	private long phoneNumber;
	
	private int idGenerator = 100;

	public Employee(String name, double salary, String address, long phoneNumber) {
		super();
		this.name = name;
		this.salary = salary;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.id = ++idGenerator;
	}
	
	public Employee(){
		super();
		this.id = ++idGenerator;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getId() {
		return id;
	}

	public int getIdGenerator() {
		return idGenerator;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + ", address=" + address
				+ ", phoneNumber=" + phoneNumber + "]";
	} 
	public void work(){
		System.out.println("I am employee!!! Doing some work");
	}
	

}
