package Inheritance;

public class Batsman extends Player {

	private int runsScored;
	private int ballsFaced;
	
	private int dismissals;
	
	public void Batsman(){
		
	}

	public Batsman(int id, int jerceyno, String name, int matches, int runsScored, int ballsFaced, int dismissals) {
		super(id,jerceyno, name, matches);
		this.runsScored = runsScored;
		this.ballsFaced = ballsFaced;
		this.dismissals = dismissals;
		
	}

	public int getRunsScored() {
		return runsScored;
	}

	public void setRunsScored(int runsScored) {
		this.runsScored = runsScored;
	}

	public int getBallsFaced() {
		return ballsFaced;
	}

	public void setBallsFaced(int ballsFaced) {
		this.ballsFaced = ballsFaced;
	}

	public int getDismissals() {
		return dismissals;
	}
	

	public void setDismissals(int dismissals) {
		this.dismissals = dismissals;
	}
	public double calStrikeRate(){
		return (double)runsScored / ballsFaced * 100;
	}
	public double calAverage(){
		return runsScored / getMatches();
	}

	@Override
	public String toString() {
		return "Batsman [runsScored=" + runsScored + ", ballsFaced=" + ballsFaced + ", dismissals=" + dismissals + "]";
	}
	
	
}