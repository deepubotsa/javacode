package loops;

public class NextPalindrome {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 121;
		System.out.println(getNextPalindrome(num));

	}

	public static String getNextPalindrome(int num) {
		String s = "";

		while (num > 0) {

			if (isPalindrome(num + 1))
				return s += num + 1;
			else
				num += 1;
		}

		return s;

	}

	public static boolean isPalindrome(int num) {
		int sum = 0, digit = 0;
		int temp = num;

		while (num > 0) {
			digit = num % 10;
			sum = sum * 10 + digit;
			num = num / 10;
		}

		if (temp == sum)
			return true;
		else
			return false;

	}

}
