package loops;

public class BreakAndContinue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		for(int i = 1;i <= 10;i ++)
		{
			if(i == 3)
			{
				continue;//all values printed except 3
				
			}
			
			System.out.println("value is:" + i);
		}
		
		
		System.out.println();
		
		//break
		
		for(int i =1;i <= 10;i ++)
		{
			if(i == 6)
			{
				break;
			}
			System.out.println("value is:" + i);
		}

	}

}
