package Arrays;

import java.util.Scanner;

public class SumOfArrayElements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int size = 0;

		Scanner scn = new Scanner(System.in);
		System.out.println("enter the size");
		size = scn.nextInt();

		int arr[] = new int[size];
		System.out.println("enter the elements");

		for (int i = 0; i < size; i++) {
			arr[i] = scn.nextInt();
		}
		System.out.println("sum is:" + sumOfElements(arr));

	}

	public static int sumOfElements(int arr[]) {
		int sum = 0;

	 	for (int i = 0; i < arr.length; i++) {
			sum += arr[i];

		}
		return sum;
	}

}
