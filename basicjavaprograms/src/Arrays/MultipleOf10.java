package Arrays;

import java.util.Scanner;

public class MultipleOf10 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int size = 0;
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the size");
		size = scn.nextInt();
		
		int arr[] = new int[size];
		System.out.println("enter the elements");

		for (int i = 0; i < size; i++) {
			arr[i] = scn.nextInt();
		}
		
		System.out.println(findMultipleOf10(arr));

	}
	public static String findMultipleOf10(int arr[]){
		String s = "";
		for(int i = 0; i < arr.length; i ++){
			if(arr[i] % 10 == 0)
				 s += arr[i] + " ";
			else
				s += (arr[i] / 10 + 1) * 10 + " ";
		}
		return s;
	}

}
