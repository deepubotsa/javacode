package Collections;

public class BubbleSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr[] = { 5, 2, 1, 4, 3 };

		System.out.println("===before sorting===");

		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}

		sort(arr);

		System.out.println("===after sorting===");

		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);

		}
	}

	public static int[] sort(int arr[]) {

		int temp;

		for (int i = 0; i < arr.length; i++) {

			for (int j = 0; j < arr.length - 1 - i; j++) {

				if (arr[j] > arr[j + 1]) {
					temp = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = temp;
				}

			}
		}

		return arr;

	}
}
