package Collections;

public class Book {
	private int id;
	private String name;
	private String author;
	private String publisher;
	
	private static int idGenerator = 100;

	public Book() {
		super();
		this.id = ++idGenerator;
	}

	public Book( String name, String author, String publisher) {
		super();
		this.id = ++idGenerator;
		this.name = name;
		this.author = author;
		this.publisher = publisher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public int getId() {
		return id;
	}
	

	@Override
	public String toString() {
		return "Book [id=" + id + ", name=" + name + ", author=" + author + ", publisher=" + publisher + "]";
	}
	
	
}
