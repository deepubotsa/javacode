package Collections;

public class Product {
	private int id;
	private int weight;
	private double price;
	public Product() {
		super();
	}
	public Product(int id, int weight, double price) {
		super();
		this.id = id;
		this.weight = weight;
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", weight=" + weight + ", price=" + price + "]";
	}
	
	

}
