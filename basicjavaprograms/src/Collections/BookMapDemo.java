package Collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class BookMapDemo {

	public static void main(String[] args) {
		// TODO Auto-erated method stub
		Map< Integer,BookMap>books = new HashMap<Integer,BookMap>();
		
		BookMap put = books.put(101, new BookMap(1,"java","abc"));
		BookMap put1 = books.put(102, new BookMap(2,"c","abc"));
		BookMap put2 = books.put(103, new BookMap(3,"c ++","abc"));
		
		System.out.println(books.put(put.getId(),put));
		books.put(put1.getId(),put1);
		
		/*BookMap b1 =  new BookMap(101,"java","abc");
		BookMap b2 = new BookMap(102,"c","abc");
		BookMap b3 =  new BookMap(103,"c ++","abc");
		
		books.put(b1.getId(), b1);
		books.put(b2.getId(), b2);
		books.put(b3.getId(), b3);*/
		
		System.out.println(books);
		
		
		

	}

}
