package Collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapDemo {
	public static void main(String[] args) {
		Map<String, Long>phoneBook = new HashMap<String,Long>();
		phoneBook.put("pradeep", 8074018466l);
		phoneBook.put("pant", 99774664672l);
		phoneBook.put("virat", 8074018466l);
		phoneBook.put("dhoni", 70982736627l);
		phoneBook.put("pradeep", 93638189918l);
		Set<String>nameSet = phoneBook.keySet();
		//System.out.println(nameSet);
		//System.out.println(phoneBook.values());
		/*System.out.println(phoneBook.remove("pradeep"));
		System.out.println(phoneBook.remove("virat", 8074018466l));*/
		
		System.out.println("============");
		
		
		System.out.println();
		
		for(String s : nameSet){
			System.out.println(s + " " + phoneBook.get(s));
			
		}
		
	 // System.out.println("value is:" +phoneBook.get("pradeep"));
	
	}
}

