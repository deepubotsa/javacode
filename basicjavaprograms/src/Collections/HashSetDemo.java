package Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;



public class HashSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Set<String> colorSet = new HashSet<>();
		
		colorSet.add("yellow");
		colorSet.add("red");
		colorSet.add("black");
		colorSet.add("white");
		colorSet.add("red");
		
		System.out.println("========");
		System.out.println(colorSet);
		
		Set<String> nameSet = new LinkedHashSet<>();
		
		nameSet.add("pradeep");
		nameSet.add("pradeep");
		nameSet.add("pradeep");
		nameSet.add("kohli");
		nameSet.add("rishabh");
		
		System.out.println("========");
		System.out.println(nameSet);
		
	}

}
