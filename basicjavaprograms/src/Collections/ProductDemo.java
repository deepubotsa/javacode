package Collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ProductDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Product> productList = new ArrayList<Product>();
		productList.add(new Product(111, 250, 25.50));
		productList.add(new Product(112, 200, 25.50));
		productList.add(new Product(113, 150, 25.50));
		productList.add(new Product(114, 180, 25.50));
		productList.add(new Product(115, 170, 25.50));
		productList.add(new Product(116, 50, 25.50));
		productList.add(new Product(117, 550, 25.50));
		productList.add(new Product(118, 500, 25.50));
		productList.add(new Product(119, 60, 25.50));
		productList.add(new Product(120, 80, 25.50));
		
		Iterator<Product>itr =productList.iterator();
		while(itr.hasNext()){
			Product p = itr.next();
			try{
				validate(p);
				
			}catch (InvalidProductException e){
				itr.remove();
				System.out.println(e);
			}
		}
		for(Product p : productList){
			System.out.println(p);
		}
		
		
}

	private static void validate(Product p) throws InvalidProductException {
		// TODO Auto-generated method stub
		if(p.getWeight() < 200){
			throw new InavlidProductException("Invalid Product!!!");
		}else{
			System.out.println("valid Product!!");
		}
		
	}
}
