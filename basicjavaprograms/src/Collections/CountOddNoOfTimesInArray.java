package Collections;

import java.util.HashMap;
import java.util.Map;

public class CountOddNoOfTimesInArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count = 0;
		int[] arr = { 1, 1, 2, 1, 3, 2, 3, 4, 4, 3, 4, 4, 6 };

		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		for (int i = 0; i < arr.length; i++) {

			if (map.containsKey(arr[i])) {

				count = map.get(arr[i]);
				map.put(arr[i], count + 1);
			} else {
				map.put(arr[i], 1);

			}

		}
		System.out.println(map);
		for(Integer i : map){
			System.out.println(i + " " + map.get(i));
		}
	}

}
