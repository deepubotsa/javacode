package Collections;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Set<String> colorSet = new TreeSet<>();
		
		colorSet.add("yellow");
		colorSet.add("red");
		colorSet.add("black");
		colorSet.add("white");
		colorSet.add("red");
		
		System.out.println("========");
		System.out.println(colorSet);
		

	}

}
