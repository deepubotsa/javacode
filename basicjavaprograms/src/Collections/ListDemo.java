package Collections;

import java.util.List;
import java.util.ArrayList;

public class ListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> yearList = new ArrayList<>();
		
		yearList.add(1996);
		yearList.add(1997);
		yearList.add(1998);
		yearList.add(1999);
		yearList.add(2000);
		//yearList.add("pradeep");
		yearList.remove(0);
		yearList.remove(1);
		yearList.remove(new Integer(2000));
		
		System.out.println("size of the list is:" + yearList.size());
		

		/*for(Integer i : yearList){
			System.out.println("size of the list is:" + yearList.size());
			
		}*/
		
			//Integer i = yearList.get("pradeep");	

	}

}
