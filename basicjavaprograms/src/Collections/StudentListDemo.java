package Collections;

import java.util.ArrayList;
import java.util.List;

import java.util.Iterator;

public class StudentListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Student> studentList = new ArrayList<>();

		Student s1 = new Student("pradeep", 90, 95, 99);
		studentList.add(s1);
		studentList.add(new Student("kohli", 80, 85, 70));
		studentList.add(new Student("sachin", 80, 85, 70));
		studentList.add(new Student("rishabh", 50, 65, 70));
		studentList.add(new Student("rishabh", 50, 65, 70));

		for (Student std : studentList) {
			System.out.println(
					"id:" + std.getId() + " " + "name:" + std.getName() + " " + "percentage:" + std.calPercentage());
		}

		System.out.println("====iterator=======");
		Iterator<Student> itr = studentList.iterator();
		
		while (itr.hasNext()) {
			Student std = itr.next();
			//System.out.println(itr.next());
			System.out.println(itr.hasNext());
			//System.out.println(std);
		   //System.out.println("id:" + std.getId() + " " + "name:" + std.getName() + " " + "percentage:" + std.calPercentage());
		}

	}

}
