package Collections;

import java.util.Scanner;

public class PrintDiagonalMatrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//int matrix[][] = new int[3][3];

		Scanner scn = new Scanner(System.in);
		System.out.println("enter the row size");
		int row = scn.nextInt();

		System.out.println("enter the col size");
		int col = scn.nextInt();
		
		int matrix[][] = new int[row][col];
		
		
		System.out.println("enter the elements");
		for (int i = 0; i < row; i++) {

			for (int j = 0; j < col; j++) {
				
				matrix[i][j] = scn.nextInt();
			}
		}
		for(int i = 0; i < matrix.length; i ++){
			
			for(int j = 0; j < matrix.length; j ++){
				
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
	

		System.out.println("===Diagonal Matrix=====");
		getDiagonal(matrix);
	}

	public static void getDiagonal(int matrix[][]) {
		for (int i = 0; i < matrix.length; i++) {

			for (int j = 0; j < matrix[i].length; j++) {

				if (i == j) {
					System.out.print(matrix[i][j] + " ");
				} else if ((i + j) == 2) {
					System.out.print(matrix[i][j] + " ");
				} else {
					System.out.print("0" + " ");
				}
			}
			System.out.println();

		}
	}
}
