package Files;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreamDemo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		FileOutputStream fio = null;
		String fname = "Pradeep is a cricketer";
		String lname = "pradeep";
		try {
			fio = new FileOutputStream("Output.txt");
			fio.write(fname.getBytes());
			fio.write(lname.getBytes());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			fio.close();
		}

	}

}
