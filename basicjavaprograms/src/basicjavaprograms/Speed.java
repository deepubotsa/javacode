package basicjavaprograms;
import java.util.Scanner;

public class Speed {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double distance;
		int hours,min,sec,totalSec;
		double meterpersec,kmperhour,mileperhours;
		
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the distance");
		distance = scn.nextDouble();
		
		System.out.println("enter the hours,min,sec");
		hours = scn.nextInt();
		min = scn.nextInt();
		sec = scn.nextInt();
		
		
		totalSec = (hours * 3660)+(min * 60)+sec;
		
		
		meterpersec = distance / totalSec;
		kmperhour = (distance / 1000.0) / (totalSec / 3600);
		mileperhours = (distance / 1609.0) / (totalSec / 3600);
		
		
		System.out.println("speed in meterpersec is:" + meterpersec);
		System.out.println("speed in kmperhour is:" + kmperhour);
		System.out.println("speed in mileperhours is:" + mileperhours);
	
		}

}
