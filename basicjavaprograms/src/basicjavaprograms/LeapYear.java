package basicjavaprograms;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int year;
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the year");
		year = scn.nextInt();

		if (year % 400 == 0) {
			System.out.println("century leap year");
		}

		else if (year % 4 == 0 && year % 100 != 0) {

			System.out.println("non century leap year");
		}
		
		else {
			System.out.println("non leap year");
		}
	}
}
