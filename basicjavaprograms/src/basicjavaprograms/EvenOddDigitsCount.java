package basicjavaprograms;

import java.util.Scanner;

public class EvenOddDigitsCount {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the num");
		int num = scn.nextInt();// 345
		
		int digit = 0, evenCount = 0, oddCount = 0;
		
		while (num > 0) {
			digit = num % 10;
			
			if (digit % 2 == 0) {
				evenCount++;
				
			} else {
				oddCount++;
			}
			num = num / 10;

		}
		System.out.println("even count of given num is" + evenCount);
		System.out.println("odd count of given num is" + oddCount);

	}

}
