package basicjavaprograms;
import java.util.Scanner;

public class Swapping {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the two values:");
		int num1 = scn.nextInt();
		int num2 = scn.nextInt();
		int temp;
		
		
		System.out.println("before swapping of two numbers:" + num1 + " " + num2);
		
		
		temp = num1;
		num1 = num2;
		num2 = temp;
		
		
		System.out.println("after swapping of two numbers:" + num1 + " " + num2);
		

	}

}
