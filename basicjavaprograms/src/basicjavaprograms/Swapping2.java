package basicjavaprograms;
import java.util.Scanner;

public class Swapping2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		System.out.println("enter the two values");
		int num1 = scn.nextInt();//30
		int num2 = scn.nextInt();//40
		
		
		num1 = num1 + num2;//70
		num2 = num1 - num2;//70-40
		num1 = num1 - num2;//70-30
		
		
		System.out.println("after swapping:" + num1 + " " + num2);

	}

}
