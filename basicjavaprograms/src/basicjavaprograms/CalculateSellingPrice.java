package basicjavaprograms;

import java.util.Scanner;

public class CalculateSellingPrice {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		
		System.out.println("enter the price of the item");
		double price = scn.nextDouble();
		
		
		if (price >= 0 && price <= 10000) {
			System.out.println("10% discount on the item and selling price is:" + (price - ((price / 100) * 10)));
		

		} else if (price >= 10000 && price <= 20000) {
			System.out.println("20% discount on the item and selling price is:" + (price - ((price / 100) * 20)));
			
		} else if (price > 20000) {
			System.out.println("25% discount on the item is:" + (price - ((price / 100) * 25)));
		}

	}

}
