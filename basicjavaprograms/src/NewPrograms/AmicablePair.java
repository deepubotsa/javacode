package NewPrograms;

public class AmicablePair {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int start = 100;
		int limit = 300000;

		System.out.println(getAmicablePair(start, limit));
	}

	public static String getAmicablePair(int start, int limit) {

		String s = "";
		int sum1 = 0, sum2 = 0;
		int i, j;

		for (i = start; i < limit; i++) {// 220
			sum1 = sumOfFactors(i);// 284

			for (j = start; j < limit; j++) {// 220
				sum2 = sumOfFactors(j);

				if (sum1 == j && sum2 == i && i != j)// sum1

					s += i + " ";

			}
		}

		return s;

	}

	public static int sumOfFactors(int num) {

		int sum = 0;
		for (int i = 1; i < num; i++) {
			if (num % i == 0)
				sum += i;
		}
		return sum;

	}
}
