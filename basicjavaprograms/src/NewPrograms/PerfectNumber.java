package NewPrograms;

public class PerfectNumber {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num = 497;
		System.out.println(calSumOfFactors(num));
		// System.out.println(isPerfect(num));
		if (calSumOfFactors(num) == num)
			System.out.println("perfect num");
		else
			System.out.println("not perfect num");

	}

	public static int calSumOfFactors(int num) {

		int sum = 0;
		for (int i = 2; i * i <= num; i++) {
			if (num % i == 0)
				sum += i + (num / i);
		}
		return sum + 1;

	}

}
