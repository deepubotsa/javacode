package NewPrograms;

import java.util.Scanner;

public class BooleanArguments {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);

		System.out.println("enter 1st argument");
		boolean a = scn.nextBoolean();

		System.out.println("enter 2nd argument");
		boolean b = scn.nextBoolean();

		System.out.println("enter 3rd argument");
		boolean c = scn.nextBoolean();

		System.out.println(checkBooleanArgs(a, b, c));
	}

	public static boolean checkBooleanArgs(boolean a, boolean b, boolean c) {

		if (a && (b || c) || (b && c))
			return true;
		else
			return false;
	}

}
