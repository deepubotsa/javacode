package NewPrograms;

public class FibonacciSeries {

	public static void main(String[] args) {
		String s = "MAN";

		System.out.println(getStringValue(s));
	}

	public static int getStringValue(String s) {

		String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int sum = 0;

		char arr[] = str.toCharArray();

		for (int i = 0; i < str.length(); i++) {

			if (arr[i] == s.charAt(0) || arr[i] == s.charAt(1) || arr[i] == s.charAt(2))

				sum += fib(i);
		}

		return sum;

	}

	public static int fib(int n) {
		if (n == 0 || n == 1)
			return n;
		else

			return fib(n - 1) + fib(n - 2);

	}
}
