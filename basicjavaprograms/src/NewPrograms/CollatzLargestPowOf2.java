package NewPrograms;

public class CollatzLargestPowOf2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(LargestPowOf2InCollatz(22));
	}

	public static int LargestPowOf2InCollatz(int num) {
		// ADD YOUR CODE HERE
		int sum1 = 0, sum2 = 0;
		int temp = num;

		while (num > 1) {

			if (num % 2 == 0) {
				num = num / 2;

				for (int i = temp; i >= 0; i--) {

					if (findPower2(i) == temp)
						return temp;

					if (findPower2(i) == num)
						sum1 += num;
				}
			} else {

				num = (num * 3) + 1;

				for (int i = temp; i >= 0; i--) {

					if (findPower2(i) == num)
						sum2 += num;
				}

			}
		}
		if (sum1 > sum2)
			return sum1;
		else
			return sum2;

	}

	public static int findPower2(int num) {
		int sum = 0;

		sum += Math.pow(2, num);

		return sum;
	}
}