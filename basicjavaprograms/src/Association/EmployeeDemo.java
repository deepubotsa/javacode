package Association;

public class EmployeeDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Address permAdd = new Address(201,"gachibowli","hyd","telengana",55);
		Employee emp1 = new Employee(101,"Ajay",2500,"testing",permAdd);
		System.out.println(emp1);
		
		Address newAdd = new Address(550,"banglore","hyd","telengana",55);
		emp1.setAdd(newAdd);
		System.out.println("=====================");
		System.out.println("house no is:" + emp1.getAdd().getHouseNo() + " " +  "city :" + emp1.getAdd().getCity() + " " + "state is:"+ emp1.getAdd().getState());

	}

}
