package Puzzle;

import java.util.Scanner;

public class Puzzle2 {
	static int row = 0;
	static int col = 0;

	public static void main(String[] args) {
		char[][] puz = { { 'A', 'B', 'C', 'D', 'E' }, { 'F', 'G', 'H', 'I', 'J' }, { 'K', 'L', 'M', 'N', 'O' },
				{ 'P', 'Q', 'R', 'S', ' ' }, { 'T', 'U', 'V', 'W', 'X' } };

		System.out.println("Input:");

		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				System.out.print((puz[i][j]));
			}
			System.out.println();
		}

		findEmpty(puz);

		Scanner scn = new Scanner(System.in);
		String str = scn.nextLine();
		doMoves(puz, str);

		if (row == -1 || col == -1) {
			System.out.println("no configaration");
		}
		isAlpahabeticOrder(puz);
	}

	public static void findEmpty(char[][] puz) {

		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				if (puz[i][j] == ' ') {
					row = i;
					col = j;
				}
			}

		}
	}

	public static void swapEmpty(char[][] puz, int row2, int col2) {
		char tmp = puz[row][col];
		puz[row][col] = puz[row2][col2];
		puz[row2][col2] = tmp;
	}

	public static boolean isValidMove(char[][] puz, char c) {
		switch (c) {
		case 'A':
			if (row == 0)
				return false;
			swapEmpty(puz, row - 1, col);
			row--;
			print(puz);
			System.out.println();
			return true;
			
		case 'B':
			if (row == 4)
				return false;
			swapEmpty(puz, row + 1, col);
			row++;
			print(puz);
			System.out.println();
			return true;
			
		case 'L':
			if (col == 0)
				return false;
			swapEmpty(puz, row, col - 1);
			col--;
			print(puz);
			System.out.println();
			return true;
			
		case 'R':
			if (col == 4)
				return false;
			swapEmpty(puz, row, col + 1);
			col++;
			print(puz);
			System.out.println();
			return true;
			
		default:
			System.out.println("Bad move: '" + c + "'");
			return false;
		}
	}

	public static void print(char[][] puz) {

		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				System.out.print((puz[i][j]) + " ");
			}
			System.out.println();
		}
	}

	public static void doMoves(char[][] puz, String str) {

		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '0')
				return;
			if (!isValidMove(puz, c)) {
				row = -1;
				col = -1;
				return;
			}
		}
	}

	public static void isAlpahabeticOrder(char[][] puz) {
		String str1 = "";
		String str2 = "ABCDEFGHIJKLMNOPQRSTUVWXY";

		for (int i = 0; i < puz.length; i++) {
			for (int j = 0; j < puz.length; j++) {
				str1 += puz[i][j];
			}
		}

		if (str1.equals(str2)) {
			System.out.println("You Win");
		}
	}

}
