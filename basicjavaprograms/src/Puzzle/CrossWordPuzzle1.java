package Puzzle;

import java.util.Scanner;

public class CrossWordPuzzle1 {

	public static void main(String[] args) {
		
		
		char[][] crossWordPuzzle = toAddWords();
		System.out.println("===display puzzle===");
		displayCrossword(crossWordPuzzle);
		
		
		
		int[][] gridNumbers = getNumber(crossWordPuzzle);
		System.out.println("====display grid numbers====");
		displayNumberGrid(gridNumbers);
		

		System.out.println(acrossWord(crossWordPuzzle, gridNumbers));
		System.out.println(downWord(crossWordPuzzle, gridNumbers));
	}

	public static void displayCrossword(char[][] crossword) {
		for (int i = 0; i < crossword.length; i++) {
			for (int j = 0; j < crossword[i].length; j++) {
				System.out.print(crossword[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static int[][] getNumber(char[][] crossword) {
		int[][] gridNumbers = new int[crossword.length][crossword[0].length];
		int count = 1;
		for (int i = 1; i < crossword.length; i++) {
			for (int j = 1; j < crossword[i].length; j++) {
				if (crossword[i][j] != '*') {
					if ((crossword[i][j - 1] == '*' || crossword[i - 1][j] == '*')) {
						gridNumbers[i][j] = count++;
					}
				}
			}
		}
		return gridNumbers;

	}

	public static void displayNumberGrid(int[][] gridNumbers) {
		for (int i = 0; i < gridNumbers.length; i++) {
			for (int j = 0; j < gridNumbers[i].length; j++) {
				System.out.print(gridNumbers[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static char[][] toAddWords() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter rows & columns");
		int row = sc.nextInt() + 2;
		int col = sc.nextInt() + 2;

		System.out.println("Enter Words");

		char[][] crossword = new char[row][col];

		for (int j = 0; j < col; j++) {
			crossword[0][j] = '*';
			crossword[row - 1][j] = '*';
		}
		for (int i = 0; i < row; i++) {
			crossword[i][0] = '*';
			crossword[i][col - 1] = '*';
		}

		for (int i = 1; i < row - 1; i++) {
			String str = sc.next();
			for (int j = 1; j < crossword[i].length - 1; j++)
				crossword[i][j] = str.charAt(j - 1);
		}

		return crossword;

	}

	public static String acrossWord(char[][] crossword, int[][] grid) {
		String str = "";
		int count = grid[1][1];
		String words = "";

		System.out.println("=====AcrossWord======");

		for (int i = 1; i < crossword.length - 1; i++) {
			for (int j = 1; j < crossword[0].length; j++) {
				if (words.length() == 0)
					count = grid[i][j];
				if (crossword[i][j] != '*') {

					words = words + crossword[i][j];

				} else {
					if (count != 0) {
						words = count + " " + words;

						str += words + "\n";
					}
					words = "";

				}
			}
		}

		return str;
	}

	public static String downWord(char[][] crossword, int[][] grid) {
		String str = "";

		int count = grid[1][1];
		String words = "";

		System.out.println("======DownWord=======");

		for (int j = 1; j < crossword.length; j++) {

			for (int i = 1; i < crossword.length; i++) {

				if (words.length() == 0)
					count = grid[i][j];
				if (crossword[i][j] != '*') {

					words = words + crossword[i][j];

				} else {
					if (count != 0) {
						words = count + " " + words;
						str += words + "\n";
					}
					words = "";

				}

			}
		}
		return str;

	}
}

