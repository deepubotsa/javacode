package Puzzle;

import java.util.Scanner;

public class puzzle1 {

	static int row = 0;
	static int col = 0;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		char puzzle[][] = { { 'T', 'R', 'G', 'S', 'J' }, { 'X', 'D', 'O', 'K', 'I' }, { 'M', ' ', 'V', 'L', 'N' },
				{ 'W', 'P', 'A', 'B', 'E' }, { 'U', 'Q', 'H', 'C', 'F' } };

		print(puzzle);
		// output

		findSpace(puzzle);
		Scanner scn = new Scanner(System.in);
		String str = scn.next();

		doMoves(puzzle, str);

		if (row < -1 || col < -1) {
			System.out.println("no configaration");
		}
	}

	public static void findSpace(char[][] puzzle) {

		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle.length; j++) {
				if (puzzle[i][j] == ' ') {
					row = i;
					col = j;
				}
			}
		}
	}

	public static void swapSpace(char[][] puzzle, int row2, int col2) {
		char tmp = puzzle[row][col];
		puzzle[row][col] = puzzle[row2][col2];
		puzzle[row2][col2] = tmp;
	}

	public static boolean isValidMove(char[][] puzzle, char c) {
		switch (c) {
		case 'A':
			if (row == 0)
				return false;
			else
				swapSpace(puzzle, row - 1, col);
			row--;
			print(puzzle);
			System.out.println();
			return true;

		case 'B':
			if (row == 4)
				return false;
			else
				swapSpace(puzzle, row + 1, col);
			row++;
			print(puzzle);
			System.out.println();
			return true;

		case 'L':
			if (col == 0)
				return false;
			else
				swapSpace(puzzle, row, col - 1);
			col--;
			print(puzzle);
			System.out.println();
			return true;

		case 'R':
			if (col == 4)
				return false;
			else
				swapSpace(puzzle, row, col + 1);
			col++;
			print(puzzle);
			System.out.println();
			return true;

		default:
			System.out.println("WRONG MOVE: '" + c + "'");
			return false;

		}
	}

	public static void doMoves(char[][] puzzle, String str) {

		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			if (c == '0')
				return;
			if (!isValidMove(puzzle, c)) {
				row = -1;
				col = -1;
				return;
			}
		}
	}

	public static void print(char[][] puzzle) {
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {
				System.out.print((puzzle[i][j]) + " ");
			}
			System.out.println();
		}
	}

}
