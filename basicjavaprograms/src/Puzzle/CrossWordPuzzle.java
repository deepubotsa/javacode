package Puzzle;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class CrossWordPuzzle {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		char[][] charPuzzle = addElements();
		System.out.println("=====dispaly puzzle====");
		display(charPuzzle);

		int numPuzzle[][] = getNumber(charPuzzle);
		// displayGridNumbers(numPuzzle);

		System.out.println(acrossWord(charPuzzle, numPuzzle));
		System.out.println(downWord(charPuzzle, numPuzzle));

	}

	public static char[][] addElements() {

		Scanner scn = new Scanner(System.in);
		System.out.println("Enter the row size");
		int row = scn.nextInt() + 2;

		System.out.println("Enter the col size");
		int col = scn.nextInt() + 2;

		System.out.println("Enter the charcters");

		char[][] puzzle = new char[row][col];

		for (int j = 0; j < col; j++) {
			puzzle[0][j] = '*';
			puzzle[row - 1][j] = '*';
		}
		for (int i = 0; i < row; i++) {
			puzzle[i][0] = '*';
			puzzle[i][col - 1] = '*';
		}

		for (int i = 1; i < row - 1; i++) {
			String str = scn.next();
			for (int j = 1; j < puzzle[i].length - 1; j++)
				puzzle[i][j] = str.charAt(j - 1);
		}

		return puzzle;

	}

	public static void display(char[][] charPuzzle) {

		for (int i = 0; i < charPuzzle.length; i++) {

			for (int j = 0; j < charPuzzle[i].length; j++) {

				System.out.print(charPuzzle[i][j] + " ");
			}
			System.out.println();
		}

	}

	public static int[][] getNumber(char[][] puzzle) {
		int[][] gridNumbers = new int[puzzle.length][puzzle[0].length];
		int count = 0;
		for (int i = 1; i < puzzle.length; i++) {
			for (int j = 1; j < puzzle[i].length; j++) {
				if (puzzle[i][j] != '*') {
					if ((puzzle[i][j - 1] == '*' || puzzle[i - 1][j] == '*')) {
						gridNumbers[i][j] = ++count;
						// System.out.println(gridNumbers[i][j]);

					}
				}
			}
		}
		return gridNumbers;

	}

	public static void displayGridNumbers(int[][] gridNumbers) {
		for (int i = 0; i < gridNumbers.length; i++) {
			for (int j = 0; j < gridNumbers.length; j++) {
				System.out.println(gridNumbers[i][j] + " ");
				System.out.println();
			}
		}
	}

	public static String acrossWord(char[][] puzzle, int[][] gridNumbers) {
		String str = "";
		int count = gridNumbers[1][1];
		String characters = "";

		System.out.println("===Across puzzle====");

		for (int i = 1; i < puzzle.length - 1; i++) {
			for (int j = 1; j < puzzle[0].length; j++) {
				if (characters.length() == 0)
					count = gridNumbers[i][j];
				if (puzzle[i][j] != '*') {
					characters += puzzle[i][j];
				} else {
					if (count != 0) {
						characters = count + "." + characters;

						str += characters + "\n";
					}
					characters = "";

				}
			}
		}

		return str;
	}

	public static String downWord(char[][] puzzle, int[][] gridNumbers) {
		String str = "";

		int count = gridNumbers[1][1];
		String characters = "";
		System.out.println("======DownWord=======");
		Map<Integer, String> map = new TreeMap<Integer, String>();

		for (int j = 1; j < puzzle.length; j++) {
			for (int i = 1; i < puzzle.length; i++) {
				if (characters.length() == 0)
					count = gridNumbers[i][j];
				if (puzzle[i][j] != '*') {
					characters += puzzle[i][j];

				} else {
					if (count != 0) {
						map.put(count, characters);
					}
					characters = "";
				}

			}
		}
		for(Integer i : map.keySet()){
			str += i + " " + map.get(i) + "\n";
		}
		return str;

	}

}