package Puzzle;

import java.awt.Point;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Othello {

	public static void main(String args[]) {
		
		char opponent = 0;

		char[][] board = { { '_', '_', '_', '_', '_', '_', '_', '_' }, { '_', '_', '_', '_', '_', '_', '_', '_' },
				{ '_', '_', '_', '_', '_', '_', '_', '_' }, { '_', '_', '_', 'W', 'B', '_', '_', '_' },
				{ '_', '_', '_', 'B', 'W', '_', '_', '_' }, { '_', '_', '_', '_', '_', '_', '_', '_' },
				{ '_', '_', '_', '_', '_', '_', '_', '_' }, { '_', '_', '_', '_', '_', '_', '_', '_' } };
		input(board);
		displayBoard(board);


		Scanner sc = new Scanner(System.in);
		char player = sc.next().charAt(0);
	
		findPlaceableLocations(player, opponent, board);
		placeMove();
		

		//othello(player, board);

	}
	private static void input(char[][] board) {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[i][j]);
			}
			System.out.println();
		}
	}
	
		private HashSet<Point>placablePositions 
{ 
	        for(int i=0;i<8;++i){
	            for(int j=0;j<8;++j){
	                if(board[i][j] == opponent){
	                    int I = i, J = j;  
	    
						if(i-1>=0 && j-1>=0 && board[i-1][j-1] == '_'){ 
	                        i = i+1; j = j+1;
	                        while(i<7 && j<7 && board[i][j] == opponent){i++;j++;}
	                        if(i<=7 && j<=7 && board[i][j] == player) placeablePositions.add(new Point(I-1, J-1));
	                    } 
	                    i=I;j=J;
	                    if(i-1>=0 && board[i-1][j] == '_'){
	                        i = i+1;
	                        while(i<7 && board[i][j] == opponent) i++;
	                        if(i<=7 && board[i][j] == player) placeablePositions.add(new Point(I-1, J));
	                    } 
	                    i=I;
	                    if(i-1>=0 && j+1<=7 && board[i-1][j+1] == '_'){
	                        i = i+1; j = j-1;
	                        while(i<7 && j>0 && board[i][j] == opponent){i++;j--;}
	                        if(i<=7 && j>=0 && board[i][j] == player) placeablePositions.add(new Point(I-1, J+1));
	                    }  
	                    i=I;j=J;
	                    if(j-1>=0 && board[i][j-1] == '_'){
	                        j = j+1;
	                        while(j<7 && board[i][j] == opponent)j++;
	                        if(j<=7 && board[i][j] == player) placeablePositions.add(new Point(I, J-1));
	                    }
	                    j=J;
	                    if(j+1<=7 && board[i][j+1] == '_'){
	                        j=j-1;
	                        while(j>0 && board[i][j] == opponent)j--;
	                        if(j>=0 && board[i][j] == player) placeablePositions.add(new Point(I, J+1));
	                    }
	                    j=J;
	                    if(i+1<=7 && j-1>=0 && board[i+1][j-1] == '_'){
	                        i=i-1;j=j+1;
	                        while(i>0 && j<7 && board[i][j] == opponent){i--;j++;}
	                        if(i>=0 && j<=7 && board[i][j] == player) placeablePositions.add(new Point(I+1, J-1));
	                    }
	                    i=I;j=J;
	                    if(i+1 <= 7 && board[i+1][j] == '_'){
	                        i=i-1;
	                        while(i>0 && board[i][j] == opponent) i--;
	                        if(i>=0 && board[i][j] == player) placeablePositions.add(new Point(I+1, J));
	                    }
	                    i=I;
	                    if(i+1 <= 7 && j+1 <=7 && board[i+1][j+1] == '_'){
	                        i=i-1;j=j-1;
	                        while(i>0 && j>0 && board[i][j] == opponent){i--;j--;}
	                        if(i>=0 && j>=0 && board[i][j] == player)placeablePositions.add(new Point(I+1, J+1));
	                    }
	                    i=I;j=J;
	                    }
	                } 
	        } 
	    } 
		
	

	public static void gameResult() {
	}
	public static void updatedScores() {
	}
	public static void getPlaceableLocations(char player,char opponent,char[][] board) {
	}
	public static void showPlaceableLocations(char player,char opponenet, char[][] board) {
	}
	public static void placeMove() {
	}
	public static void displayBoard(char[][] board) {
	}
	
	
}
