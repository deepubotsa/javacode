package Puzzle;

public class Point {
	int x,y;

	public Point() {
		super();
	}

	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	
	@Override
	public String toString() {
		return " [" + x + ", " + y + "]";
	}

	@Override
	public int hashCode(){
		return Integer.parseInt(x + "" + y);
	}
	

}
