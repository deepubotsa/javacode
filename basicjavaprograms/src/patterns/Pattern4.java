package patterns;

public class Pattern4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int star = 5;
		for (int i = 1; i <= star; i++) {
			for (int k = 1; k <= 5 - i; k++) {
				System.out.print(" ");
			}

			for (int j = 1; j <= i; j++) {
				System.out.print("* ");
			}
			System.out.println();

		}

	}
}
