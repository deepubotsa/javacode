package Movie;

public class Movies {
	
	private String name;
	private String director_name;
	private int duration;
	private int releasedYear;
	public Movies() {
		super();
	}
	public Movies(String name, String director_name, int duration, int releasedYear) {
		super();
		this.name = name;
		this.director_name = director_name;
		this.duration = duration;
		this.releasedYear = releasedYear;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDirector_name() {
		return director_name;
	}
	public void setDirector_name(String director_name) {
		this.director_name = director_name;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public int getReleasedYear() {
		return releasedYear;
	}
	public void setReleasedYear(int releasedYear) {
		this.releasedYear = releasedYear;
	}
	@Override
	public String toString() {
		return "Movies [name=" + name + ", director_name=" + director_name + ", duration=" + duration
				+ ", releasedYear=" + releasedYear + "]";
	}
	
	
	

}
