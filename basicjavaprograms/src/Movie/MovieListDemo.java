package Movie;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class MovieListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scn = new Scanner(System.in);
		System.out.println("enter your choice:");
		int choice = scn.nextInt();
		
		List<Movies> movieList = new ArrayList<Movies>();

		movieList.add(new Movies("srimanthudu", "koratala siva", 2016_2017, 2017));
		movieList.add(new Movies("bahubali", "rajmouli", 2015_2016, 2015));
		movieList.add(new Movies("temper", "poori", 2016_2017, 2017));
		movieList.add(new Movies("rangasthalam", "sukumar", 2017_2018, 2018));
		movieList.add(new Movies("nuvve nuvve", "trivikram", 2005_2006, 2007));

	
		switch(choice){
		case 1:Collections.sort(movieList, new SortByYear());
				System.out.println("===sort by year======");
				for (Movies m : movieList) {
					System.out.println(m);
				}
				break;
		case 2:System.out.println();
				System.out.println("===sort by name=====");
				Collections.sort(movieList, new SortByName());

				for (Movies m : movieList) {
					System.out.println(m);
				}
				break;
		case 3:System.out.println();
				System.out.println("===sort by Director_name=====");
				Collections.sort(movieList, new SortByDirector_name());
		
				for(Movies m : movieList){
				System.out.println(m);
				}
				break;
		case 4:System.out.println("exit");
				break;
			
	}
		
	}

}
