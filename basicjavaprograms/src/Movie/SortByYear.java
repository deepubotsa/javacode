package Movie;

import java.util.Comparator;

public class SortByYear implements Comparator<Movies> {

	@Override
	public int compare(Movies m1, Movies m2) {
		// TODO Auto-generated method stub
		if (m1.getReleasedYear() > m2.getReleasedYear())
			return 1;
		else if (m1.getReleasedYear() < m2.getReleasedYear())
			return -1;

		else
			return 0;
	}

}
