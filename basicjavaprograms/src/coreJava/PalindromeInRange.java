package coreJava;

public class PalindromeInRange {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int num1 = 100;
		int num2 = 1000;
		int num = 121;

		System.out.println(isPalindrome(num));
		System.out.println("palindrome in range:" + palindromeInRange(num1,num2));

	}

	public static boolean isPalindrome(int num) {
		int sum = 0, rem = 0;
		int temp = num;
		while (num > 0) {
			
			rem = num % 10;
			sum = sum * 10 + rem;
			num = num / 10;
		}

			if (sum == temp)
				return true;
			else
				return false;
		
	}
	public static String palindromeInRange(int start,int limit)
	{
		String result = "";
		
		for(int i = start;i <= limit; i ++)
		{
			if(isPalindrome(i))
			result += i + " ";
			
		}
		return result;
	}
}