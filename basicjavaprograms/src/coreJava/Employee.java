package coreJava;

public class Employee {
	private int id;
	private int id_generator = 100;
	private String first_name;
	private String last_name;
	private double salary;
	private double anual_salary;
	public static String company = "Talent sprint";
	
	
	
	
	public Employee(String first_name, String last_name, double salary) {
		super();
		this.id = ++ id_generator ;
		this.first_name = first_name;
		this.last_name = last_name;
		this.salary = salary;
	}
	
	public Employee()
	{
		this.id = ++ id_generator ;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getFirst_name() {
		return first_name;
	}



	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}



	public String getLast_name() {
		return last_name;
	}



	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}



	public double getSalary() {
		return salary;
	}



	public void setSalary(double salary) {
		this.salary = salary;
	}
    
	public double cal()
	{
		return double anual_salary = 12 * salary;
	}


	public String toString()
	{
		return " id: " + id + " first_name: " + first_name + " last_name: " + last_name + " salary: " + salary + " company: " + company;
	}
	
	
	}


