package coreJava;

import java.util.Scanner;

public class Student {

	private int id;
	private String name;
	private String address;
	public static String collegeName ;
	private static int id_generator = 100;
	

	public Student()  //when the default values executed
	{
	 this.id = ++id_generator;	
	}
	public Student(int id,String name,String address)
	{
		 
		this.id = id;
		this.name = name;
		this.address= address;
	}
	
	
	
	public Student(String name,String address)
	{
		
		this.id = ++id_generator;
		this.name = name;
		this.address= address;
	}
	
	
	
	public void display(){
		System.out.println( "id:" + id + "  name :" + name + "  address :" + address + "  collegename:" + collegeName ); 
	}
	
	
}
