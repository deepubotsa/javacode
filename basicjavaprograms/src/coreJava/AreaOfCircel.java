package coreJava;

public class AreaOfCircel {
	private double pi ;
	private double radius;
	
	
	public  AreaOfCircel(double pi, double radius) {
		super();
		this.pi = pi;
		this.radius = radius;
	}


	public double getPi() {
		return pi;
	}


	public void setPi(double pi) {
		this.pi = pi;
	}


	public double getRadius() {
		return radius;
	}


	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double area()
	{
		return pi * radius * radius;
	}
	public static AreaOfCircel compare()
	

}
