package coreJava;

public class Circle {

	private double pi ;
	private double radius ;
	
	
    public double area()
    {
     return pi * radius * radius;	
    }
    
    public double circumference()
    {
    	return 2 * pi * radius;
    }
    
    
    public double getpi()
    {
      return this.pi;
    }
    
    public double getradius()
    {
      return this.radius;
    }
    
    public void setpi(double pi)
    {
    	 this.pi = pi;
    }
    
    public void setradius(double radius)
    {
    	 this.radius = radius;
    }
   
}
    

